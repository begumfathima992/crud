const USER=require('../models/User')
exports.index = (req,res) => {
    res.json({message:"Welcome to UsersDB"});
}
exports.register= (req,res) => {
    const {username,password,email,contact} = req.body;
   const newUser=USER({
       username,
       password,
       email,
       contact
   })
   USER.findOne({email:email})
   .then(user=>{
       if(user){ return res.json({message:"user already exist"})} else{
           newUser.save().then(()=> res.json({ message:"user created"}))
           .catch(err => res.json({eror:err.toString()}))
       }
   }).catch(err=> res.json({error:err.toString()}))
}
exports.users=(req,res)=>{
    USER.find().then(users=> res.json({data:users}))
    .catch(err=>res.json({error:err.toString()}))

}
exports.user = (req,res) => {
    const {email} = req.body;

    USER.findOne({email:email})
    .then(user => {
        if(user) {
            res.json({data:user})
        } else {
            res.json({message:"User not found !"})
        }
    })
    .catch(err => res.json({error:err.toString()}))
}
exports.delete=(req,res)=>{
    const {email}=req.body;
    USER.findOne({email:email})
    .then(()=>res.json({mssg:"user deleted"}))
    .catch(err=>res.json({error:toString()}))
}
exports.update=(req,res)=>{
    const {username,password,email,contact}=req.body;
    const updateuser={
        username,
        password,
        email,
        contact
    }
    USER.findOne({email:email})
    .then(user=>{
        if(user) {
           USER.updateOne({email:email},{$set:{...updateuser}})
           .then(()=>res.json({mssg:"user updated"}))
            .catch((err)=>res.json({error:err.toString()}))
        }
        else{
res.json({mssg:"user couldnt find"})
        }
    })
}